import { Component } from '@angular/core';
import { NavbarMenuItemService, NavbarMenuItem } from './shared/top-navbar/state';
import { Router } from '@angular/router';
import { Session, SessionMap, SessionService } from './session/state';

@Component({
  selector: 'vf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  items: NavbarMenuItem[] = [
    {
      text: 'FIX',
      order: 1,
      link: '/fix',
      children: [
        { text: 'FIX Dictionaries', order: 1, link: '/dictionaries' },
        { text: 'Order Book Consultation Services', order: 2, link: '/consultation-services' },
        { text: 'FIX Order Book', order: 3, link: '/order-book' }
      ]
    },
    {
      text: 'Trading Server',
      order: 2,
      link: '/trading-server',
      children: [
        { text: 'FIX IN Trading Server', order: 1, link: '/fix-in' },
      ]
    },
    {
      text: 'Alert',
      order: 3,
      link: '/alert',
      children: [
        { text: 'Alert Configurations', order: 1, link: '/configurations' },
        { text: 'Alerts', order: 2, link: '/list' }
      ]
    },
    {
      text: 'Archive',
      order: 4,
      link: '/archive',
      children: [
        { text: 'FIX Order Archive', order: 1, link: '/fix-order' },
        { text: 'MDS Archive', order: 2, link: '/mds' }
      ]
    },
    {
      text: 'Tools',
      order: 5,
      link: '/tools',
      children: [
        { text: 'Logging', order: 1, link: '/logging' },
        { text: 'Import Routes', order: 2, link: '/import-routes' },
        { text: 'Recording', order: 3, link: '/recording' },
        { text: 'Query', order: 4, link: '/query' },
      ]
    },
  ];

  sessions: SessionMap = {
    'FIX': [
      {
        name: 'FIX1',
        type: 'FIX',
        protocol: 'FIX',
        status: 'CONNECTED'
      },
      {
        name: 'FIX_ACCEPTOR',
        type: 'FIX',
        protocol: 'FIX',
        disabled: true,
        status: 'DISABLED'
      },
      {
        name: 'FIX_MOCK',
        type: 'FIX',
        protocol: 'FIX',
        disabled: true,
        status: 'DISABLED'
      }
    ],
    'SLE': [
      {
        name: 'SLE_MOCK',
        type: 'SLE',
        protocol: 'SLE',
        disabled: true,
        status: 'DISABLED'
      },
      {
        name: 'SLE_TEST',
        type: 'SLE',
        protocol: 'SLE',
        disabled: true,
        status: 'DISABLED'
      },
      {
        name: 'sle69',
        type: 'SLE',
        protocol: 'SLE',
        disabled: true,
        status: 'DISCONNECTED'
      }
    ],
    'FILE': [
      {
        name: 'test2',
        type: 'FILE',
        status: 'DISCONNECTED'
      }
    ],
    'HTTP': [
      {
        name: 'LOB',
        type: 'HTTP',
        protocol: 'JSONQUERY',
        status: 'CONNECTED'
      },
      {
        name: 'Test',
        type: 'HTTP',
        protocol: 'JSONQUERY',
        status: 'CONNECTED'
      }
    ],
    'MongoDB': [
      {
        name: 'MONGODB_LOG',
        type: 'MongoDB',
        status: 'CONNECTED'
      }
    ],
    'SOCKETS': [
      {
        name: 'test_socket',
        type: 'SOCKETS',
        protocol: 'JSONQUERY',
        status: 'CONNECTED'
      }
    ],
    'SQL': []
  };

  constructor(
    navbarMenuItemService: NavbarMenuItemService,
    sessionService: SessionService,
    private router: Router
  ) {
    navbarMenuItemService.addAll(this.items);
    sessionService.addSessionMap(this.sessions);
  }

  goTo(link: string) {
    this.router.navigateByUrl(link);
  }
}
