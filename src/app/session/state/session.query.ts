import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { SessionStore, SessionState } from './session.store';
import { Session, SessionMap } from './session.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SessionQuery extends QueryEntity<SessionState, Session> {

  constructor(protected store: SessionStore) {
    super(store);
  }

  selectAllAsSessionMap(): Observable<SessionMap> {
    return this.selectAll()
      .pipe(
        map(sessions => {
          const sessionMap: SessionMap = {};
          sessions.forEach(session => {
            if (!sessionMap[session.type]) {
              sessionMap[session.type] = [];
            }
            sessionMap[session.type].push(session);
          })
          return sessionMap;
        })
      );
  }

  selectSessionTypes(): Observable<string[]> {
    return this.selectAllAsSessionMap()
      .pipe(
        map(map => Object.keys(map))
      );
  }
}
