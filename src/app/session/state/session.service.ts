import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { SessionStore } from './session.store';
import { HttpClient } from '@angular/common/http';
import { Session, createSession, SessionMap } from './session.model';

@Injectable({ providedIn: 'root' })
export class SessionService {

  constructor(private store: SessionStore,
              private http: HttpClient) {
  }

  get() {
    // this.http.get().subscribe((entities: ServerResponse) => {
      // this.sessionStore.set(entities);
    // });
  }

  add(session: Session) {
    this.store.add(session.id ? session : createSession(session));
  }

  addAll(items: Session[]) {
    items.forEach(item => this.add(item));
  }

  addSessionMap(sessionMap: SessionMap) {

    Object.keys(sessionMap)
      .forEach(sessionType => {
        sessionMap[sessionType].forEach(session => {
          this.add(session);
        });
        if (!sessionMap[sessionType] || sessionMap[sessionType].length === 0) {
          this.add({
            name: '',
            type: sessionType
          });
        }
      });
  }

}
