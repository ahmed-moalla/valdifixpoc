import { ID, guid } from '@datorama/akita';

export type SessionStatus = 'CONNECTED' | 'DISCONNECTED' | 'DISABLED';

export interface Session {
  id?: ID;
  name: string;
  type: string;
  protocol?: string;
  disabled?: boolean;
  status?: SessionStatus;
}

export interface SessionMap {
  [type: string]: Session[];
}

export function createSession(params: Partial<Session>): Session {
  return {
    id: guid(),
    ...params
  } as Session;
}
