import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SessionBrowserComponent } from './session-browser/session-browser.component';

const routes: Routes = [
  {
    path: '',
    component: SessionBrowserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SessionRoutingModule { }
