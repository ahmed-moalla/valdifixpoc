import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionRoutingModule } from './session-routing.module';
import { SessionBrowserComponent } from './session-browser/session-browser.component';
import { MaterialModule } from '../shared/material/material.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SessionBrowserComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    SessionRoutingModule
  ]
})
export class SessionModule { }
