import { Component, OnInit } from '@angular/core';
import { SessionMap, SessionQuery, Session, SessionStatus } from '../state';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material';
import { map } from 'rxjs/operators';

interface SessionTableModel {
  status: SessionStatus;
  name: string;
  type: string;
  protocol: string;
}

@Component({
  selector: 'vf-session-browser',
  templateUrl: './session-browser.component.html',
  styleUrls: ['./session-browser.component.css']
})
export class SessionBrowserComponent implements OnInit {

  sessionMap$: Observable<SessionMap>;
  dataSource: MatTableDataSource<SessionTableModel>;
  columnLabels = {
    status: 'Status',
    name: 'Name',
    type: 'Type',
    protocol: 'Protocol',
  };
  constructor(private sessionQuery: SessionQuery) { }

  ngOnInit() {
    this.sessionMap$ = this.sessionQuery.selectAllAsSessionMap();
    this.sessionQuery.selectAll()
      .pipe(
        map<Session[], SessionTableModel[]>(sessions => 
          sessions.map(session => ({
            status: session.status,
            name: session.name,
            type: session.type,
            protocol: session.protocol
          })))
      )
      .subscribe(sessionModels => this.dataSource = new MatTableDataSource(sessionModels));
  }

  onTypeClick(type: string) {
    console.log('Type', type);
  }

  onItemClick(item: Session) {
    console.log('Item', item);
  }

}
