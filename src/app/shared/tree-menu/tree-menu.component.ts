import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

export interface TreeMenuData {
  [type: string]: any[]
}

interface TreeMenuType {
  typeName: string;
  expanded: boolean;
  empty: boolean;
}

@Component({
  selector: 'vf-tree-menu',
  templateUrl: './tree-menu.component.html',
  styleUrls: ['./tree-menu.component.css']
})
export class TreeMenuComponent implements OnInit {

  @Input() data: TreeMenuData;
  @Input() displayProperty: string;
  @Output() itemClicked = new EventEmitter<any>();
  @Output() typeClicked = new EventEmitter<string>();
  types: TreeMenuType[];

  constructor() { }

  ngOnInit() {
    this.types = Object.keys(this.data)
      .map(key => {
        return {
          typeName: key, 
          expanded: true,
          empty: !this.data[key] || this.data[key].length === 0 || this.data[key][0].name === ''
        }
      });
  }

  toggleExpand(type: TreeMenuType) {
    type.expanded = !type.expanded;
  }
}
