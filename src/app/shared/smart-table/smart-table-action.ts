export interface SmartTableAction {
    icon: string;
    iconColor?: string;
    tooltip?: string;
    callback: (row: any) => void;
}
