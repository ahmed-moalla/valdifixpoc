import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, Sort} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SmartTableAction} from './smart-table-action';

@Component({
  selector: 'vf-smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.css'],
  animations: [
    trigger('sortState', [
      state('asc', style({
        transform: 'rotate(0deg)'
      })),
      state('desc', style({
        transform: 'rotate(180deg)'
      })),
      state('none', style({
        opacity: '0'
      })),
      transition('none => asc', animate('150ms ease-in')),
      transition('asc => desc', animate('150ms ease-out')),
      transition('desc => none', animate('150ms ease-out'))
    ])
  ]
})
export class SmartTableComponent implements OnInit, AfterViewInit {

  displayedColumns: string[];
  sorting: { [s: string]: 'none' | 'asc' | 'desc'; } = {};
  @Input() dataSource;
  @Input() columnLabels: string[];
  @Input() actions: SmartTableAction[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  ngOnInit() {
    this.setupDisplayedColumns();
    this.setupSortingState();
  }

  setupDisplayedColumns() {
    this.displayedColumns = Object.keys(this.columnLabels);
    if (this.actions && this.actions.length > 0) {
      this.displayedColumns.push('actions');
    }
  }

  setupSortingState() {
    Object.keys(this.columnLabels).forEach(key => {
      this.sorting[key] = 'none';
    });
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  sortData(sort: Sort) {
    if (sort.direction === '') {
      this.sorting[sort.active] = 'none';
    } else {
      this.sorting[sort.active] = sort.direction;
    }
  }

}

