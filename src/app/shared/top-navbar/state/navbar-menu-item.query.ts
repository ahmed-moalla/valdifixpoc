import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { NavbarMenuItemStore, NavbarMenuItemState } from './navbar-menu-item.store';
import { NavbarMenuItem } from './navbar-menu-item.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class NavbarMenuItemQuery extends QueryEntity<NavbarMenuItemState, NavbarMenuItem> {

  constructor(protected store: NavbarMenuItemStore) {
    super(store);
  }

  selectAllOrdered(): Observable<NavbarMenuItem[]> {
    return this.selectAll()
      .pipe(
        map(items => items.sort((i1, i2) => i1.order - i2.order))
      );
  }

}
