import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { NavbarMenuItem } from './navbar-menu-item.model';

export interface NavbarMenuItemState extends EntityState<NavbarMenuItem> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'navbar-menu-items' })
export class NavbarMenuItemStore extends EntityStore<NavbarMenuItemState, NavbarMenuItem> {

  constructor() {
    super();
  }

}

