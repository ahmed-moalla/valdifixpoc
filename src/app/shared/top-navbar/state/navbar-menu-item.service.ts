import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { NavbarMenuItemStore } from './navbar-menu-item.store';
import { HttpClient } from '@angular/common/http';
import { NavbarMenuItem, createNavbarMenuItem } from './navbar-menu-item.model';

@Injectable({ providedIn: 'root' })
export class NavbarMenuItemService {

  constructor(private store: NavbarMenuItemStore,
    private http: HttpClient) {

  }

  get() {
    // this.http.get().subscribe((entities: ServerResponse) => {
    // this.navbarMenuItemStore.set(entities);
    // });
  }

  add(item: NavbarMenuItem) {
    this.store.add(item.id ? item : createNavbarMenuItem(item));
  }

  addAll(items: NavbarMenuItem[]) {
    items.forEach(item => this.add(item));
  }

}
