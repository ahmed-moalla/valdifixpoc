import { ID, guid } from '@datorama/akita';

export interface NavbarMenuItem {
  id?: ID;
  text: string;
  link: string;
  order: number;
  children?: NavbarMenuItem[];
}

export function createNavbarMenuItem(params: Partial<NavbarMenuItem>) {
  return {
    id: guid(),
    ...params
  } as NavbarMenuItem;
}
