import { NgModule } from '@angular/core';

import { TopNavbarComponent } from './top-navbar.component';
import { MaterialModule } from '../material/material.module';
import { CommonModule } from '@angular/common';
import { MenuItemComponent } from './menu-item/menu-item.component';

@NgModule({
    imports: [MaterialModule, CommonModule],
    exports: [TopNavbarComponent],
    declarations: [TopNavbarComponent, MenuItemComponent],
    providers: [],
})
export class TopNavbarModule { }
