import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavbarMenuItem } from '../state';

@Component({
  selector: 'vf-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent {

  @Input() item: NavbarMenuItem;
  @Output() click = new EventEmitter<string>();

}
