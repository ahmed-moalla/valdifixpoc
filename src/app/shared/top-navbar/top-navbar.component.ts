import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavbarMenuItemQuery, NavbarMenuItem } from './state';
import { Observable } from 'rxjs';

@Component({
  selector: 'vf-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {

  menuItems$: Observable<NavbarMenuItem[]>;
  @Output() linkClicked = new EventEmitter<string>();

  constructor(
    private query: NavbarMenuItemQuery
  ) { }

  ngOnInit() {
    this.menuItems$ = this.query.selectAllOrdered();
  }

  goTo(link: string) {
    if (typeof link !== 'string') return;
    this.linkClicked.emit(link);
  }

}
