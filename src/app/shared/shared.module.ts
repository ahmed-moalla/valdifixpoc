import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { TopNavbarModule } from './top-navbar/top-navbar.module';
import { TreeMenuComponent } from './tree-menu/tree-menu.component';
import { SmartTableComponent } from './smart-table/smart-table.component';

const components = [
  TreeMenuComponent,
  SmartTableComponent
];

const modules = [
  MaterialModule,
  TopNavbarModule
];

@NgModule({
  declarations: components,
  imports: [
    CommonModule,
    ...modules
  ],
  exports: [
    ...modules,
    ...components,
  ],
  providers: [],
})
export class SharedModule { }
